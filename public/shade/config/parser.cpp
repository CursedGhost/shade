
#include "parser.h"

namespace shade
{


void CParser::ThrowEx( const KvContext* ctx, const char* sub, const char* fmt, va_list va ) const
{
}
KvNode* CParser::Find( KvNode* kv, idtype_t id ) const
{
	return nullptr;
}
KvLexer* CParser::Lexer() const
{
	return nullptr;
}
void CParser::Process( void* data, const char* text, HandlerFn pfn, const KvContext& ctx, ErrorFn err ) const
{
}
int CParser::Integer( KvNode* kv, const int bounds[] ) const
{
	const char* str = kv->value;
	int i = atoi( str );

	// Result can only be zero if the str is "0"
	if ( !i && str[0]!='0' )
		Throw( kv, "Parser::Integer()", "Not a number!" );

	// Bounds checking if requested
	if ( bounds && ( i<bounds[0] || i>bounds[1] ) )
		Throw( kv, "Parser::Integer()", "Must be within [%d,%d]!", bounds[0], bounds[1] );

	return i;
}
void CParser::LargeInt( KvNode* kv, __int64& out ) const
{
}
unsigned int CParser::IntArray( KvNode* kv, int* out, int count ) const
{
	return 0;
}
double CParser::Number( KvNode* kv ) const
{
	return 0;
}
unsigned int CParser::Vector( KvNode* kv, float* out, int count ) const
{
	return 0;
}

unsigned int CParser::Enum( KvNode* kv, const enum_t* e ) const
{
	return 0;
}
bool CParser::Bool( KvNode* kv ) const
{
	return false;
}

color_t CParser::Color( KvNode* kv ) const
{
	return color::Black;
}
	

}
