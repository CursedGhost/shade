#ifndef HGUARD_SHADE_CONFIG_LEXER
#define HGUARD_SHADE_CONFIG_LEXER
#pragma once

//----------------------------------------------------------------
// KeyValues format
//----------------------------------------------------------------
// Config files parsed as my custom key:value; format.
//

#include "tools/yalloc.h"
#include "keyvalues.h"

namespace shade
{

class KvLexer
{
public:
	virtual ~KvLexer();

	// Parse character data from str to end in a given context
	// Returns false on error
	virtual bool Parse( const char* str, const char* end, const KvContext& ctx );
	// If you prefer exceptions, just call this if Parse() fails
	virtual void Throw( );

	// Get the version string
	inline const char* Version() const { return _version; }
	// Get the nodes
	inline KvNode* Nodes() const { return _nodes; }
	// Get the current parsing context (in case of error)
	inline const KvContext& Context() const { return _context; }

protected:
	char* _version;
	KvNode* _nodes;
	KvContext _context;
	tools::Yalloc<4000> _alloc;
};

}

#endif // !HGUARD_SHADE_CONFIG_LEXER
