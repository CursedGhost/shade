#ifndef HGUARD_SHADE_RENDER_BEZIER
#define HGUARD_SHADE_RENDER_BEZIER
#pragma once

#include "shared.h"

namespace shade
{

// Computes a point on the cubic bezier curve.
SHADEAPI void SHADECC PtCubicBezier( const point_t pts[4], float t, float* xy );

// Fast SSE version for internal use.
// Input points are stored in two __m128 registers: x1 y1 x2 y2 and x3 y3 x4 y4
void SSE_CubicBezier( const __m128 apts[2], float t, float* xy );

}

#endif // !HGUARD_SHADE_RENDER_BEZIER
