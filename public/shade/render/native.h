#ifndef HGUARD_SHADE_RENDER_NATIVE
#define HGUARD_SHADE_RENDER_NATIVE
#pragma once

#include "../base.h"
#include "../time.h"

// Some stuff to avoid including windows.h...
typedef struct HWND__* HWND;
typedef struct HINSTANCE__* HINSTANCE;
typedef struct tagMSG MSG;

namespace shade
{

class IRender2D;
class BaseUI;
class CNative;


// Create a native backed by DX9 rendering
SHADEAPI CNative* SHADECC CreateNativeDX9( HINSTANCE hInst, const char_t* lpszWindowName );



class CNative : public IUnknown
{
public:
	typedef IUnknown BaseClass;

public:
	// Create a new window
	CNative( HINSTANCE hInst, const char_t* lpszWindowName );
	// Destruct the window
	virtual ~CNative();

public:
	// Do the message loop
	virtual int MessageLoop();
	virtual bool WndProc( MSG& msg );
	inline IRender2D* Graphics() { return cg; }
	virtual void Paint();
	
	virtual bool Show( bool show = true );
	inline bool Hide() { return Show( false ); }

	virtual void FrameWait();

protected:
	HINSTANCE mhInst;
	HWND mhWnd;
	BaseUI* gui;
	IRender2D* cg;
	Timer timer;
	fltime_t minft;
};


}

#endif // !HGUARD_SHADE_GUI_NATIVE
