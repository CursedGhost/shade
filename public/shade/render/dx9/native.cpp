
#include "native.h"
#include "sdk.h"

namespace shade
{
CNative* SHADECC CreateNativeDX9( HINSTANCE hInst, const char_t* lpszWindowName )
{
	return new dx9::CNativeDX9( hInst, lpszWindowName );
}

namespace dx9
{
	

CNativeDX9::CNativeDX9( HINSTANCE hInst, const char_t* lpszWindowName ) :
	BaseClass( hInst, lpszWindowName ),
	mpD3D( WrapCreateD3D() ),
	render( mhWnd, mpD3D, mpDevice )
{
}
CNativeDX9::~CNativeDX9()
{
	if ( mpDevice ) { mpDevice->Release(); mpDevice = NULL; }
	if ( mpD3D ) { mpD3D->Release(); mpD3D = NULL; }
	::FreeLibrary( hmD3D9 );
}
IDirect3D9* CNativeDX9::WrapCreateD3D()
{
	if ( !CreateD3D() )
		throw std::exception( "CNativeDX9::CNativeDX9() Failed to Create D3D!" );
	return mpD3D;
}
bool CNativeDX9::CreateD3D()
{
	// Dynamically load d3d9.dll
	hmD3D9 = ::LoadLibrary( TEXT("d3d9.dll") );
	if ( !hmD3D9 )
		return false;

	// Init D3DX9
	if ( !InitImports() )
		return false;

	// Create the D3D
	const meta::declpfn<IDirect3D9* (__stdcall*)( UINT )> pfn( ::GetProcAddress( hmD3D9, "Direct3DCreate9" ) );
	mpD3D = pfn( D3D_SDK_VERSION );
	if ( !mpD3D )
		return false;
	
	// Create the Device
	mrcClient = rect_t(0,0,0,-1);
	if ( !UpdateParams() )
		return false;

	HRESULT hr = mpD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, mhWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &mParams, &mpDevice );
	return SUCCEEDED(hr);
}
bool CNativeDX9::UpdateParams()
{
	RECT rc;
	::GetClientRect( mhWnd, &rc );

	// If we're not minimized & size changed, update the parameters
	if ( !( ( (rc.right-rc.left)>0 && (rc.right-rc.left)!=mrcClient.width ) ||
		    ( (rc.bottom-rc.top)>0 && (rc.bottom-rc.top)!=mrcClient.height ) ) )
	{
		 return false;
	}

	mParams.BackBufferWidth = rc.right;
	mParams.BackBufferHeight = rc.bottom;
	
	// First time calling us
	if ( mrcClient.height==-1 )
	{
		mParams.BackBufferFormat = D3DFMT_X8R8G8B8;
		mParams.BackBufferCount = 0;

		mParams.MultiSampleType = D3DMULTISAMPLE_8_SAMPLES;
		mParams.MultiSampleQuality = 0;

		mParams.SwapEffect = D3DSWAPEFFECT_DISCARD;
		mParams.hDeviceWindow = mhWnd;
		mParams.Windowed = TRUE;

		mParams.EnableAutoDepthStencil = FALSE;
		mParams.AutoDepthStencilFormat = D3DFMT_UNKNOWN;
		mParams.Flags = 0;

		mParams.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
		mParams.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

		// We start hidden so...
		suspend = true;
	}

	mrcClient.left = rc.left;
	mrcClient.width = rc.right - rc.left;
	mrcClient.top = rc.top;
	mrcClient.height = rc.bottom - rc.top;

	sizing = false;
	return true;
}
void CNativeDX9::Manage()
{
	// Wait for the previous frame to complete its timeslice
	FrameWait();

	HRESULT hr;
	hr = mpDevice->TestCooperativeLevel();

	// Check if we need to update (window resize or device lost)
	if ( !UpdateParams() && SUCCEEDED(hr) )
		return;
	render.OnLostDevice();
		
	// Wait until device becomes available
	if ( hr==D3DERR_DEVICELOST )
	{
		do
		{
			SleepEx( 100, TRUE );
			hr = mpDevice->TestCooperativeLevel();
		}
		while ( hr!=D3DERR_DEVICENOTRESET );
	}

	// Reset the device
	hr = mpDevice->Reset( &mParams );
	assert( SUCCEEDED(hr) );
	render.OnResetDevice();

	// Ensure everything is correct
	assert( SUCCEEDED(mpDevice->TestCooperativeLevel()) );
}
bool CNativeDX9::WndProc( MSG& msg )
{
	// Suspend painting while minimized / sizing / moving
	// GOD these messages are so fucking unreliable...
	switch ( msg.message )
	{
	case WM_ENTERSIZEMOVE:
		sizing = true;
		break;
	case WM_EXITSIZEMOVE:
		sizing = false;
		break;
	case WM_PAINT:
		if ( sizing || suspend )
		{
			// FIXME! CPU usage skyrockets when minimized (suspend=true)...
			HRESULT hr = mpDevice->Present( NULL, NULL, NULL, NULL );
			assert( SUCCEEDED(hr) );
			return true;
		}
		break;

	// Suspend while minimized
	case WM_SIZE:
		if ( msg.wParam==SIZE_MINIMIZED )
			suspend = true;
		else if ( !sizing && msg.wParam==SIZE_RESTORED )
			suspend = false;
		break;
	}

	return BaseClass::WndProc( msg );
}
void CNativeDX9::Paint()
{
	Manage();

	mpDevice->BeginScene();
	mpDevice->Clear( 0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(255,196,196,196), 0.0f, 0 );

	render.Begin();

	float time = static_cast<float>( FloatTime() );
	
	//render.cache.Begin( render.currentContext, render.cache.BTYPE_TRIS, 3, 1, HASH("Diffuse") );
	//render.cache.AddIndex( 1 );
	//render.cache.AddIndex( 2 );
	//render.cache.AddIndex( 0 );
	//render.cache.AddVert( 1.0f, 9.0f, D3DCOLOR_ARGB(255,255,0,255) );
	//render.cache.AddVert( 50.0f+sin(time)*80.0f, 90.0f+cos(time)*40.0f, D3DCOLOR_ARGB(255,255,255,0) );
	//render.cache.AddVert( 100.0f+sin(time+2)*23.0f, 5.0f+cos(time-2)*67.0f, D3DCOLOR_ARGB(255,0,255,255) );
	//render.cache.End();

	//render.SetPenColor( color::Amethyst );
	//render.MoveTo( 78, 80 );
	//render.SetPenColor( color::Azure );
	//render.LineTo( 231, 23 );

	point_t curve[5] = {
		point_t( 20*4, 80*4 ),
		point_t( 30*4, 20*4 ),
		point_t( 70*4, 30*4 ),
		point_t( 80*4, 80*4 ),
		point_t( 50*4, 130*4 ),
	};
	render.SetPenColor( color::Aquamarine );
	render.DrawPoly( curve, 5, false );
	render.SetPenColor( color::HotPink );

	//fltime_t begin = FloatTime();
	//float xy[2];
	//for ( int i = 0; i<10000; ++i )
	//	PtCubicBezier( curve, 0.5f, xy );
	//fltime_t delta = FloatTime() - begin;
	//printf( "Delta %f\n", static_cast<float>(delta) );

	render.DrawSpline( curve, 5, static_cast<int>((sin(time)+cos(time))*256) );

	render.SetPenColor( color::LavenderBlush );
	render.DrawRoundRect( rect_t(50,80,100,33), 20, 20 );

	render.End();

	mpDevice->EndScene();
	mpDevice->Present( NULL, NULL, NULL, NULL );
}
bool CNativeDX9::Show( bool show )
{
	// Suspend while hidden
	BaseClass::Show( show );
	suspend = !show;
	return true;
}

}
}
