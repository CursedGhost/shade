#ifndef HGUARD_SHADE_RENDER_DX9_SDKDX9
#define HGUARD_SHADE_RENDER_DX9_SDKDX9
#pragma once

#include <WinGDI.h>
// Enable usage of GUIDs; alt link with dxguid.lib
#include <InitGuid.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <d3dx9core.h>
#include <d3dx9effect.h>
#include <dxerr.h>
#include "../undef.h"

#include "meta/declpfn.h"

namespace shade
{
namespace dx9
{

struct d3dx9_t
{
	static d3dx9_t& get();

	//meta::declpfn<HRESULT (WINAPI*)(
	//	LPDIRECT3DDEVICE9	pDevice,
	//	LPD3DXSPRITE*		ppSprite )> CreateSprite;

	//meta::declpfn<HRESULT (WINAPI*)(
	//	LPDIRECT3DDEVICE9	pDevice,
	//	LPD3DXLINE*			ppLine)> CreateLine;

	//meta::declpfn<HRESULT (WINAPI*)(
	//	LPDIRECT3DDEVICE9	pDevice,  
	//	INT					Height,
	//	UINT				Width,
	//	UINT				Weight,
	//	UINT				MipLevels,
	//	BOOL				Italic,
	//	DWORD				CharSet,
	//	DWORD				OutputPrecision,
	//	DWORD				Quality,
	//	DWORD				PitchAndFamily,
	//	LPCSTR				pFaceName,
	//	LPD3DXFONT*			ppFont)> CreateFontA;
		
	//meta::declpfn<HRESULT (WINAPI*)(
	//	LPCSTR				pSrcData,
	//	UINT				SrcDataLen,
	//	CONST D3DXMACRO*	pDefines,
	//	LPD3DXINCLUDE		pInclude,
	//	LPCSTR				pFunctionName,
	//	LPCSTR				pProfile,
	//	DWORD				Flags,
	//	LPD3DXBUFFER*		ppShader,
	//	LPD3DXBUFFER*		ppErrorMsgs,
	//	LPD3DXCONSTANTTABLE* ppConstantTable)> CompileShader;
		
	meta::declpfn<HRESULT (WINAPI*)(
		LPDIRECT3DDEVICE9	pDevice,
		LPCVOID				pSrcData,
		UINT				SrcDataLen,
		const D3DXMACRO*	pDefines,
		LPD3DXINCLUDE		pInclude,
		DWORD				Flags,
		LPD3DXEFFECTPOOL	pPool,
		LPD3DXEFFECT*		ppEffect,
		LPD3DXBUFFER*		ppCompilationErrors)> CreateEffect;

	meta::declpfn<HRESULT (WINAPI*)(
		LPD3DXEFFECT		pEffect,
		BOOL				EnableColorCodes,
		LPD3DXBUFFER*		ppDisassembly )> DisassembleEffect;
		
	meta::declpfn<HRESULT (WINAPI*)(
		LPDIRECT3DDEVICE9	pDevice,
		LPCSTR				pSrcFile,
		UINT				Width,
		UINT				Height,
		UINT				MipLevels,
		DWORD				Usage,
		D3DFORMAT			Format,
		D3DPOOL				Pool,
		DWORD				Filter,
		DWORD				MipFilter,
		D3DCOLOR			ColorKey,
		D3DXIMAGE_INFO*		pSrcInfo,
		PALETTEENTRY*		pPalette,
		LPDIRECT3DTEXTURE9*	ppTexture)> CreateTextureFromFileExA;

	//meta::declpfn<HRESULT (WINAPI*)(
	//	LPDIRECT3DDEVICE9	pDevice,
	//	UINT*				pWidth,
	//	UINT*				pHeight,
	//	UINT*				pNumMipLevels,
	//	DWORD				Usage,
	//	D3DFORMAT*			pFormat,
	//	D3DPOOL				Pool)> CheckTextureRequirements;
};
struct gdi_t
{
	static gdi_t& get();

	meta::declpfn<HFONT (WINAPI*)(
		const LOGFONTA* lpgf )> CreateFontIndirectA;

	meta::declpfn<HGDIOBJ (WINAPI*)(
		HDC hdc,
		HGDIOBJ h )> SelectObject;

	meta::declpfn<BOOL (WINAPI*)(
		HGDIOBJ ho )> DeleteObject;

	meta::declpfn<DWORD (WINAPI*)(
		HDC hdc,
		LPCWSTR lpstr,
		int c,
		LPWORD pgi,
		DWORD fl)> GetGlyphIndicesW;

	meta::declpfn<DWORD (WINAPI*)(
		HDC hdc,
		UINT uChar,
		UINT uFormat,
		LPGLYPHMETRICS lpgm,
		DWORD cbBuffer,
		LPVOID lpvBuffer,
		const MAT2* lpmat2 )> GetGlyphOutlineW;

	meta::declpfn<BOOL (WINAPI*)(
		HDC hdc,
		LPTEXTMETRICW lptm)> GetTextMetricsW;
		
	meta::declpfn<BOOL (WINAPI*)(
		HDC hdc,
		LPCWSTR lpString,
		int c,
		LPSIZE psizl )> GetTextExtentPoint32W;
};

inline bool InitImports()
{
	return true;
	//return LookupImports( d3dx9_t::get() ) && LookupImports( gdi_t::get() );
}

}
}

#endif // !HGUARD_SHADE_RENDER_DX9_SDKDX9
