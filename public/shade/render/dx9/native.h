#ifndef HGUARD_SHADE_RENDER_DX9_NATIVE
#define HGUARD_SHADE_RENDER_DX9_NATIVE
#pragma once

#include "../native.h"
#include "shared.h"
#include "sdk.h"
#include "dx9.h"

namespace shade
{
namespace dx9
{

class CNativeDX9 : public CNative
{
public:
	typedef CNative BaseClass;

	CNativeDX9( HINSTANCE hInst, const char_t* lpszWindowName );
	virtual ~CNativeDX9();

	IDirect3D9* WrapCreateD3D();
	bool CreateD3D();
	bool UpdateParams();
	void Manage();

	virtual bool WndProc( MSG& msg );
	virtual void Paint();
	virtual bool Show( bool show );

private:

	// DirectX
	HMODULE hmD3D9;
	IDirect3D9* mpD3D;
	IDirect3DDevice9* mpDevice;

	// Management
	D3DPRESENT_PARAMETERS mParams;
	rect_t mrcClient;
	bool sizing;
	bool suspend;

	// Renderer
	CRender render;

};

}
}

#endif // !HGUARD_SHADE_RENDER_DX9_NATIVE
