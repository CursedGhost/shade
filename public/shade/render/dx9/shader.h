#ifndef HGUARD_SHADE_RENDER_DX9_SHADER
#define HGUARD_SHADE_RENDER_DX9_SHADER
#pragma once

#include "shared.h"
#include "cache.h"

namespace shade
{
namespace dx9
{

// Y U NO LET ME DO THIS
//class CCache;
//struct CCache::Batch;

class CShader
{
public:
	CShader();
	~CShader();

	// Compile the effects file that will be used for this shader
	void Compile( const char_t* effect );

	void Setup( const CCache::Batch* b );
	unsigned int Pass( unsigned int pass );
	void Finish();
	void Begin( fltime_t time );
	void End();

	void Lost();
	void Reset();

	D3DXHANDLE Tech( idtype_t id );

	void Dump( const char* file );

public:
	bool doclip;
	unsigned int passes;

	IDirect3DVertexDeclaration9* decl;
	ID3DXEffect* efx;
};

}
}

#endif // !HGUARD_SHADE_RENDER_DX9_SHADER
