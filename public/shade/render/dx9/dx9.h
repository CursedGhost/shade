#ifndef HGUARD_SHADE_RENDER_DX9_DX9
#define HGUARD_SHADE_RENDER_DX9_DX9
#pragma once

#include "shared.h"
#include "../render2d.h"

#include "shader.h"
#include "cache.h"

namespace shade
{
namespace dx9
{

class CRender : public IRender2D
{
public:
	typedef IRender2D BaseClass;

	CRender( HWND hWnd, IDirect3D9* d3d, IDirect3DDevice9* dev );
	virtual ~CRender();

	virtual void Begin();
	virtual void End();

			void OnLostDevice();
			void OnResetDevice();

	virtual void ContextPush( const context_t& ctx, context_t& old );
	virtual void ContextPop( const context_t& old );
	virtual void ContextGet( context_t& ctx );

	// Shapes and stuff

	virtual void SetPenColor( color_t clr );
	virtual void SetPenStyle( unit width, int pattern, int caps );
	virtual void MoveTo( unit x, unit y );
	virtual void LineTo( unit x, unit y );
	virtual void DrawRect( const rect_t& rc );
	virtual void DrawRoundRect( const rect_t& rc, unit sx, unit sy );
	virtual void DrawEllipse( const rect_t& rc );
	virtual void DrawBezierQuad( const point_t pts[3] );
	virtual void DrawBezierCubed( const point_t pts[4] );
	color_t lineColor;
	color_t lineColor2;
	unit lineX;
	unit lineY;
	int lineSegments;

	virtual void FillRect( const rect_t& rc );
	virtual void FillConvex( const point_t* pts, const float* uvs, int count );
	virtual void FillPolygon( const Polygon& poly );
	virtual void FillBezierQuad( const point_t pts[3], const float uvs[6] );
	//virtual void ComputePolyUV( const point_t* pts, int count, float scaleX, float scaleY, float rotate, const point_t& topleft, float* uv );
	color_t brushColor;

public:
	HWND				mhWnd;
	bool				mbIsEx;
	IDirect3D9*			d3d;
	IDirect3DDevice9*	dev;

	ID3DXLine*			mpLine;

public:
	CCache				cache;
	CShader				shader;

	context_t			currentContext;

	perf_t				perf;
};

}
}

#endif // !HGUARD_SHADE_RENDER_DX9_DX9
