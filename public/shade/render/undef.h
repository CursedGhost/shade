
// This is C++, not C so we don't need/want these macros
// Must be outside header guard to enforce at all times!

#ifdef DrawText
#	undef DrawText
#endif // DrawText
#ifdef CreateFont
#	undef CreateFont
#endif // CreateFont
#ifdef LoadImage
#	undef LoadImage
#endif // LoadImage
#ifdef TextOut
#	undef TextOut
#endif // TextOut
#ifdef CreateDesktop
#	undef CreateDesktop
#endif // CreateDesktop
#ifdef THIS
#	undef THIS
#endif // THIS
#ifdef INTERFACE
#	undef INTERFACE
#	define INTERFACE class __declspec(novtable)
#endif // INTERFACE
