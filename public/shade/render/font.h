#ifndef HGUARD_SHADE_RENDER_FONT
#define HGUARD_SHADE_RENDER_FONT
#pragma once

//------------------------------------------------
// Text
//------------------------------------------------
//
// Font resources.
//
// Each font can have an optional identifier exposing it to be found by that name.
// Font resources cannot be freed explicitly, once in the system it'll remain there until the renderer is destroyed.
// Fonts can be recreated by calling the UpdateFont function.
//
// DrawText positions the text in the given rectangle using specified alignment.
//
// MeasureText measures the size of a piece of text in pixels.
// while MeasureChars computes how many of the given characters will fit in the given width (note: stops at count, null or a newline is found).
//
// TODO:
//  Support color codes inlined in the string.
//

#include "shared.h"

namespace shade
{

class IRenderFont
{
public:

	// Management
	virtual hfont CreateFont( const logfont_t& log, idtype_t id = 0 ) = 0;
	virtual hfont FindFont( idtype_t id ) = 0;
	virtual bool UpdateFont( hfont handle, const logfont_t& log ) = 0;
	
	virtual void SetFont( hfont handle, color_t fg, color_t bg = color::Black, fntflag_t flags = FONT_CLEAR ) = 0;

	// Draw text
	virtual void DrawText( const char* str, const rect_t& pos, align_t align = ALIGN_TOPLEFT, int count = -1 ) = 0;
	virtual void DrawText( const wchar_t* str, const rect_t& pos, align_t align = ALIGN_TOPLEFT, int count = -1 ) = 0;

			void DrawText( const char* str, int xpos, int ypos, align_t align = ALIGN_TOPLEFT, int count = -1 );
			void DrawText( const wchar_t* str, int xpos, int ypos, align_t align = ALIGN_TOPLEFT, int count = -1 );

	// Purpose is to start drawing where you left off or something...
	// SetFont() is still required
	virtual void SetTextPos( int xpos, int ypos ) = 0;
	virtual void TextOut( const char* str, int count = -1 ) = 0;
	virtual void TextOut( const wchar_t* str, int count = -1 ) = 0;

	// Measurements
	virtual void MeasureText( const char* str, int& width, int& height, int count = -1 ) = 0;
	virtual void MeasureText( const wchar_t* str, int& width, int& height, int count = -1 ) = 0;
	virtual int MeasureChars( const char* str, int width, int count = -1 ) = 0;
	virtual int MeasureChars( const wchar_t* str, int width, int count = -1 ) = 0;
};



inline void IRenderFont::DrawText( const char* str, int xpos, int ypos, align_t align = ALIGN_TOPLEFT, int count = -1 )
{
	DrawText( str, rect_t(xpos,ypos,0,0), align, count );
}
inline void IRenderFont::DrawText( const wchar_t* str, int xpos, int ypos, align_t align = ALIGN_TOPLEFT, int count = -1 )
{
	DrawText( str, rect_t(xpos,ypos,0,0), align, count );
}

}

#endif // !HGUARD_SHADE_RENDER_FONT
