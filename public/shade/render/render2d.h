
#ifndef HGUARD_SHADE_RENDER_RENDER2D
#define HGUARD_SHADE_RENDER_RENDER2D
#pragma once

#include "shared.h"

namespace shade
{


class Polygon;


class IRender2DShape
{
public:

	// Set the pencil color
	virtual void SetPenColor( color_t clr ) = 0;
	// Set the pencil style
	virtual void SetPenStyle( unit width = 0, int pattern = 0, int caps = 0 ) = 0;
	// Move the cursor
	virtual void MoveTo( unit x, unit y ) = 0;
	// Draw a line from cursor to coordinates
	virtual void LineTo( unit x, unit y ) = 0;
	// Draw a line
	inline void DrawLine( unit x1, unit y1, unit x2, unit y2 )
	{
		MoveTo( x1, y2 );
		LineTo( x2, y2 );
	}
	// Draw connecting lines, loop to close the loop
	inline void DrawPoly( const point_t* pts, int count, bool loop )
	{
		MoveTo( pts[0].x, pts[0].y );
		for ( auto it = pts+1, end = pts+count; it!=end; ++it )
		{
			LineTo( it->x, it->y );
		}
		if ( loop )
		{
			LineTo( pts[0].x, pts[0].y );
		}
	}
	// Draw a rectangle
	virtual void DrawRect( const rect_t& rc ) = 0;
	// Draw a rounded rectangle
	virtual void DrawRoundRect( const rect_t& rc, unit sx, unit sy ) = 0;
	// Draws an ellipsis
	virtual void DrawEllipse( const rect_t& rc ) = 0;

	// Draws a quadratic bezier
	virtual void DrawBezierQuad( const point_t pts[3] ) = 0;
	// Draws a spline of quad beziers
	inline void DrawSplineQuad( const point_t* pts, int count )
	{
		count -= 2;
		for ( int i = 0; i<count; i+=2 )
		{
			DrawBezierQuad( pts );
		}
	}
	template< unsigned N >
	inline void DrawSplineQuad( const point_t (&pts)[N] )
	{
		DrawSplineQuad( pts, N );
	}
	// Draws a cubic bezier
	virtual void DrawBezierCubed( const point_t pts[4] ) = 0;
	inline void DrawSplineCubed( const point_t* pts, int count )
	{
		count -= 3;
		for ( int i = 0; i<count; i+=3 )
		{
			DrawBezierCubed( pts );
		}
	}
	template< unsigned N >
	inline void DrawSplineCubed( const point_t (&pts)[N] )
	{
		DrawSplineCubed( pts, N );
	}
	// Hermite spline
	inline void DrawHermitePiece( const point_t& a, const point_t& u, const point_t& v, const point_t& b )
	{
		// Transform hermite to bezier
		point_t curve[4] = {
			point_t( a ),
			point_t( a.x + u.x/3, a.y + u.y/3 ),
			point_t( b.x - v.x/3, b.y - v.y/3 ),
			point_t( b )
		};
		DrawBezierCubed( curve );
	}
	// Cardinal spline
	inline void DrawSpline( const point_t* pts, int count, unit tension )
	{
		// FIXME! This assumes unit is integer!
		point_t u(0,0), v;
		unit t = 256 - tension;

		int i, n = count-2;
		for ( i = 0; i<=n; ++i )
		{
			if ( i==n )
			{
				// On last iteration outgoing velocity to zero
				v = point_t(0,0);
			}
			else
			{
				// Required 24.8 fixed point for accuracy...
				v.x = (( ( pts[i+2].x - pts[i].x )>>1 )*t)>>8;
				v.y = (( ( pts[i+2].y - pts[i].y )>>1 )*t)>>8;
			}
			DrawHermitePiece( pts[i], u, v, pts[i+1] );
			u = v;
		}
	}
};




class IRender2D : public IUnknown, public IRender2DShape
{
public:
	typedef IUnknown BaseClass;
	
	//------------------------------------------------
	// Management
	//------------------------------------------------
	
	// Start drawing
	virtual void Begin() = 0;
	// End drawing
	virtual void End() = 0;
	// Get performance variables
	//virtual void Performance() = 0;


	//------------------------------------------------
	// Drawing context
	//------------------------------------------------

	// Adjust the context, old records are on the stack
	virtual void ContextPush( const context_t& ctx, context_t& old ) = 0;
	virtual void ContextPop( const context_t& old ) = 0;
	// Directly manipulate the context
	virtual void ContextGet( context_t& ctx ) = 0;

};

}

#endif // !HGUARD_SHADE_RENDER_RENDER2D
