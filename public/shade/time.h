#ifndef HGUARD_SHADE_TIME
#define HGUARD_SHADE_TIME
#pragma once

#include "base.h"

namespace shade
{
	
typedef ::time_t time_t;
typedef unsigned int mstime_t;
typedef double fltime_t;


// Query super accurate system time.
// Frequency may vary, represents how much time needs to increment before a whole second has passed.
SHADEAPI void SHADECC QueryTime( time_t& time, time_t& freq );
// Get milisecond accurate time information, can store up to 49.7 days.
SHADEAPI mstime_t SHADECC MsecTime();
// Get seconds since launch with sub-milisecond precision for thousands of years.
SHADEAPI fltime_t SHADECC FloatTime();


class Timer
{
public:
	// Syncrhonize timer with now.
	void Sync();
	// Get time passed since last sync in ms.
	mstime_t Msec() const;
	// Get time passed since last sync in seconds.
	fltime_t Delta() const;

	// Adds a number of seconds to the time
	// Used primarily for fps limiting corrections
	void BaseAdd( fltime_t s );

private:
	time_t _time;
};






inline void Timer::Sync()
{
	time_t freq;
	QueryTime( _time, freq );
}
inline mstime_t Timer::Msec() const
{
	time_t time, freq;
	QueryTime( time, freq );
	return static_cast<unsigned int>( ( time - _time ) / ( static_cast<unsigned int>( freq ) / 1000 ) );
}
inline fltime_t Timer::Delta() const
{
	time_t time, freq;
	QueryTime( time, freq );
	return static_cast<fltime_t>( static_cast<double>( time - _time ) / static_cast<double>( freq ) );
}
inline void Timer::BaseAdd( fltime_t s )
{
	time_t add, freq;
	QueryTime( add, freq );
	add = static_cast<time_t>( static_cast<double>( freq ) * static_cast<double>( s ) );
	_time += add;
}

}

#endif // !HGUARD_SHADE_TIME
