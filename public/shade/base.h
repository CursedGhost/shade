#ifndef HGUARD_SHADE_BASE
#define HGUARD_SHADE_BASE
#pragma once

//----------------------------------------------------------------
// shade/base.h Basic stuff
//----------------------------------------------------------------
// Provides basic definitions and abstractions

#include <exception>
#include <cassert>
#include <string>

#include "libex.h"
#include "tools/enumlogic.h"
#include "tools/enumstring.h"
#include "tools/state.h"
#include "tools/va_buf.h"
#include "hash/hash.h"
#include "color/argb.h"
#include "color/names.h"
#include "meta/strcrypt.h"

#ifdef _DEBUG
#	define SHADE_DEBUG
#endif // _DEBUG

// Library usage
//
// VGUI_EXPORTS -> For compiling as DLL
// VGUI_IMPORTS -> Import from DLL
// VGUI_STATIC  -> Link with static library

#if defined(SHADE_EXPORTS)
#	define SHADEAPI extern "C" __declspec(dllexport)
#elif defined(SHADE_IMPORTS)
#	define SHADEAPI extern "C" __declspec(dllimport)
#elif defined(SHADE_STATIC)
#	define SHADEAPI
#else
#	error Please specify how you wish to use this library!
#	define SHADEAPI
#endif
#define SHADECC __fastcall

// Select graphics unit, only supports int or float!
#ifndef SHADE_UNIT
#define SHADE_UNIT int
#define SHADE_UNIT_INT
#else
#if !defined(SHADE_UNIT_INT) && !defined(SHADE_UNIT_FLOAT)
#error Unknown shade unit selected!
#endif
#endif // SHADE_UNIT

// Checks if S can be casted to T at compile time
#define TYPE_CHECK( T, S ) while (false) { *(static_cast<T* volatile*>(0)) = static_cast<T*>(static_cast<S*>(0)); }


namespace shade
{


// Identifiers are hashed names
typedef unsigned long idtype_t;

// String types
typedef char char_t;
typedef ::std::char_traits<char_t> char_traits;
typedef ::std::basic_string<char_t> string_t;

// Colors
typedef ::color::argb color_t;

// Declarations
struct KvNode;
struct KvContext;


// class IUnknown
// All shade objects are reference counted.
INTERFACE IUnknown
{
protected:
	virtual ~IUnknown() {}
	// Keep your constructor protected!
	// Create a dedicated creator of objects that use new!
	IUnknown() : _refc(1) {}
public:
	inline void Release()
	{
		if ( --_refc<=0 )
			delete this;
	}
	inline void AddRef()
	{
		++_refc;
	}
	//inline virtual idtype_t QueryType( idtype_t id = 0 ) const = 0
	//{
	//	idtype_t self = VGUI_TYPE(CLS);
	//	return ( !id || id==self )?self:0;
	//}

private:
	int _refc;
};

// RAII handles for IUnknown objects
// MAY NEVER CONTAIN NULL HANDLES!
template< typename T >
class Handle
{
public:
	//// Give Handle private access to all Handle templated privates.
	//template< typename S > friend class Handle;

	// Initialize from raw ptr, do not AddRef()
	// BE CAREFUL ABOUT WHO CALLS THIS!
	Handle( T* t ) : ptr(t)
	{
		assert( ptr!=nullptr );
	}
	// Initialize from a handle, does AddRef()
	Handle( const Handle<T>& rhs ) : ptr(rhs.ptr)
	{
		// We copied a handle, increment reference
		ptr->AddRef();
	}
	//// Move semantics, how do I do this!
	//Handle( Handle<T>&& rvalue ) : ptr( std::move(rvalue) )
	//{
	//	ptr->AddRef();
	//}
	~Handle()
	{
		// Release as needed
		assert( ptr!=nullptr );
		ptr->Release();
	}

	inline T* operator-> () const { assert(ptr); return ptr; }
	inline T& operator* () const { assert(ptr); return *ptr; }
	inline operator T* () const { return ptr; }

	// Returns this object as a compatible handle type.
	template< typename S >
	inline Handle<S>& cast()
	{
		TYPE_CHECK( T, S );
		return *reinterpret_cast<Handle<S>*>( this );
	}

private:
	T* ptr;
};



// Error handling
class Error : public std::exception
{
public:
	inline virtual const char* what() const
	{
		if ( msg.length()==0 )
			format( msg );
		return msg.c_str();
	}
	virtual void format( string_t& str ) const = 0;

protected:
	mutable string_t msg;
};

}

#endif // !HGUARD_SHADE_BASE
