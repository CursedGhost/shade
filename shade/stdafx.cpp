// stdafx.cpp : source file that includes just the standard includes
// shade.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#include "../public/shade/time.cpp"
#include "../public/shade/localize.cpp"

#include "../public/shade/config/parser.cpp"
#include "../public/shade/config/lexer.cpp"

#include "../public/shade/render/native.cpp"
#include "../public/shade/render/render2d.cpp"
#include "../public/shade/render/bezier.cpp"
#include "../public/shade/render/libdx9.cpp"
