
// Vertex shader input
struct VSIN
{
	float2 coord  : POSITION;
	float4 color  : COLOR0;
	float2 tex    : TEXCOORD0;
};
// Vertex shader output / Pixel shader input
struct VSOUT
{
	float4 coord  : POSITION;
	float4 color  : COLOR0;
	float2 tex    : TEXCOORD0;
	float2 pos    : TEXCOORD1;
};


//----------------------------------------------------------------
// Global constants
//----------------------------------------------------------------

uniform float time;

uniform float2 screen;
uniform float2x4 project;

uniform sampler2D input;
uniform sampler2D mask;

#define GREY_SATURATION 0.2f


//----------------------------------------------------------------
// Vertex Shaders
//----------------------------------------------------------------

float4 vstransform( float2 coord, const float2 shift )
{
	float2 final = ( mul( project, float4(coord.x,coord.y,1,1) ) + shift ) * 2 / screen;
	float4 hcs =
	{
		final.x - 1,
		1 - final.y,
		0,
		1,
	};
	return hcs;
}

VSOUT vsfix( VSIN data )
{
	VSOUT vert;
	
	// Transform the 2D pixel coordinates into homogeneous clip space
	// Offset coordinates slightly to perfectly map pixels and texels
	// See http://msdn.microsoft.com/en-us/library/windows/desktop/bb219690%28v=vs.85%29.aspx
	const float2 shift = { -0.5f, -0.5f };
    
	// Copy the other data
    vert.coord = vstransform( data.coord, shift );
	vert.color = data.color;
	vert.tex = data.tex;
	vert.pos = vert.coord.xy;
	
	return vert;
}
VSOUT vsdef( VSIN data )
{
	VSOUT vert;
    const float2 shift = { 0.0f, 0.0f };
	
	// Copy the other data
    vert.coord = vstransform( data.coord, shift );
	vert.color = data.color;
	vert.tex = data.tex;
	vert.pos = vert.coord.xy;
	
	return vert;
}


//----------------------------------------------------------------
// Fragment Shaders
//----------------------------------------------------------------

// Compute the luminance of a color
float lum( float4 color )
{
	return color.r*0.3f + color.g*0.59f + color.b*0.11f;
}
/*// Pixel blurring
float4 blur( float2 uv )
{
	float4 clr = { 0.0f, 0.0f, 0.0f, 0.0f };
	float2 xy;
	for ( xy.x = -1.0f; xy.x<=1.0f; xy.x+=1.0f )
	{
		for ( xy.y = -1.0f; xy.y<=1.0f; xy.y+=1.0f )
		{
			float4 c = tex2D( input, uv + xy/insize );
			clr += c/9.0f;
		}
	}
	return clr;
}*/

// Diffuse fragment shader
float4 diffuse( VSOUT vert ) : COLOR
{
	float4 px = vert.color;
	return px;
}

// Simple fragment shader
float4 modulate( VSOUT vert ) : COLOR
{
	float4 px;
	px = tex2D(input,vert.tex) * vert.color;
	return px;
}

// Colorizing shader (white remains white, black remains black)
float4 colorize( VSOUT vert ) : COLOR
{
    float4 c = tex2D( input, vert.tex );
    float l = lum( c );
    if ( l<0.5f )
        c.rgba = lerp( float4(0,0,0,c.a), c, l*2 );
    else
        c.rgba = lerp( c, float4(1,1,1,c.a), (l-0.5)*2 );
    return c;
}

// Greyscale fragment shader
float4 grey( VSOUT vert ) : COLOR
{
	float4 clr = tex2D(input,vert.tex) * vert.color;
	
	// Greyscale by luminance
	float g = lum( clr );
	float4 grey = float4( g, g, g, clr.a );
	
	// Apply saturation factor
	return lerp( grey, clr, GREY_SATURATION );
}

// Pink checker board :3
float4 error( VSOUT vert ) : COLOR
{
	float4 px;
	px.rgba = ( sin( (vert.pos.x*20.0f + sin(time)*10)*(cos(time)+2) )*cos( (vert.pos.y*20.0f + cos(time)*10)*(sin(time)+2) )/2.0f ) + 1.0f;
	float4 pink = { 1.0f, 0.0f, 0.75f, 0.2f };
	return px*pink;
}


//----------------------------------------------------------------
// Techniques
//----------------------------------------------------------------
technique Error
{
	pass p0
	{
		PixelShader = compile ps_2_0 error();
		VertexShader = compile vs_2_0 vsdef();
	}
}
technique Diffuse
{
	pass p0
	{
		PixelShader = compile ps_2_0 diffuse();
		VertexShader = compile vs_2_0 vsdef();
	}
}
technique TexturedSimple
{
	pass p0
	{
		PixelShader = compile ps_2_0 modulate();
		VertexShader = compile vs_2_0 vsfix();
	}
}
technique Colorize
{
    pass p0
    {
        PixelShader = compile ps_2_0 colorize();
        VertexShader = compile vs_2_0 vsfix();
    }
}
